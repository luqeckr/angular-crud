import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  data: any
  result: any
  access_token
  header
  constructor(
    private _http: HttpClient,
    private _router: Router
  ) { }

  ngOnInit(): void {
    if (!this.getToken()) {
      this._router.navigate(['/login'])
    }

    this.data = {

      "nip":"123456789",

      "full_name":"Erna Widhiastuti",

      "nick_name":"Erna",

      "birth_date":"1987-04-04 00:00:00",

       "address":"Jl. Mulawarman No 123",

       "phone":"1234567890",

      "mobile":"0987654321",

       "email":"erna@gmail.com"

    }

    
  }

  createData() {
    this.setHeader()
    this._http.post('https://api.smartbiz.id/api/demo/employee', this.data, this.header)
      .subscribe(res => {
        console.log(res)
        this.result = res
      }, err => {
        this.result = err
        if (err.status == 401) this._router.navigate(['/login'])
      })
  }

  setHeader() {
    this.header = {
      headers: new HttpHeaders()
        .set('Authorization',  `Bearer ${this.getToken()}`)
    }
  }

  getToken() {
    return localStorage.getItem('access_token')
  }

}
