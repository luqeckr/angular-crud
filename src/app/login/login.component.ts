import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  identity: string
  password: string
  constructor(
    private _http: HttpClient,
    private _router: Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log(this.identity, this.password)
    this._http.post('https://api.smartbiz.id/api/login', {
      identity: this.identity,
      password: this.password
    }).subscribe((res:any) => {
      console.log(res)
      localStorage.setItem('access_token', res.data.token.access_token)
      localStorage.setItem('refresh_token', res.data.token.access_token)
      this._router.navigate(['/'])
    })
  }

}
